# Relationship Estimation from Ancient DNA (READ) #

This is a repository containing the scripts used for READ. READ is a method to infer the degree of relationship (up to second degree, i.e. nephew/niece-uncle/aunt, grandparent-grandchild or half-siblings) for a pair of low-coverage individuals.

The approach is described in our paper: 

Jose Manuel Monroy Kuhn, Mattias Jakobsson, Torsten G�nther: "Estimating genetic kin relationships in prehistoric populations" Plos ONE (2018)

doi: https://doi.org/10.1371/journal.pone.0195491

http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0195491

## Requirements ##

* Python 2.7 or higher
* GNU R (assumes the command Rscript can be used)
* Unix-like operating system

## Changelog ##

* 01/09/2023: Added better handling of input file issues (e.g. no overlapping SNPs). Help can be displayed with -h, version with -v Tagged version 1.01. (user requests)
* 08/05/2018: Option to change window size added (user request)
* 09/02/2018: Classification thresholds updated. The new thresholds are based on theoretical expectations and performed well in simulations.
* 09/02/2018: Graphical output of results added

## How to use READ? ##

### Obtaining READ ###

You'll need to clone this repository (git clone https://tguenther@bitbucket.org/tguenther/read.git) or download the two scripts READ.py and READscript.R from [this site](https://bitbucket.org/tguenther/read/src). You might want to make the file READ.py executable (chmod +x).

READ is also available through Bioconda. Thanks to Thiseas Lamnidis for creating the [bioconda recipe](https://bioconda.github.io/recipes/kinship-read/README.html#package-kinship-read).

### Preparation of input files ###

READ assumes pseudohaploid data which means that all individuals in the input file are coded as completely homozygous. It is a quite common approach in aDNA research to obtain such data by randomly sampling a sequencing read per SNP site (using a pre-defined panel of SNP sites). Implementations of this approach can be found in [ANGSD](http://www.popgen.dk/angsd/index.php/Haploid_calling) and [SequenceTools](https://github.com/stschiff/sequenceTools). Common file formats for genotype data are VCF, Eigenstrat or various Plink formats. [Plink](http://pngu.mgh.harvard.edu/~purcell/plink/), [vcftools](http://vcftools.sourceforge.net/man_latest.html), [PGDSpider](http://www.cmpg.unibe.ch/software/PGDSpider/) or  [convertf](https://github.com/argriffing/eigensoft/tree/master/CONVERTF) can be used to convert between these formats.

The input for READ is a pair of files in Plink's TPED/TFAM format containing information on the individuals and their genotypes. Please note that you should use a single file pair per test population (you can use Plink's --keep to filter individuals from TPED/TFAM files). READ assumes that missing data is coded as "0". [Plink](http://pngu.mgh.harvard.edu/~purcell/plink/) can be used to filter and manipulate files. If you are starting from an all-sites VCF file, please use [vcftools](http://vcftools.sourceforge.net/man_latest.html) and the option --plink-tped to prepare the input files. If that VCF file is an all-sites VCF, please also exclude non-polymorphic and low frequency variants (e.g. --maf 0.01).

### Running READ ###

Assume you have READ and a pair of files example.tped and example.tfam in your current directory, then you can simply run READ like this:

python READ.py example

This runs the READ script in default settings. The results of your READ analysis can be found in the two files READ_results and meansP0_AncientDNA_normalized. The main result file is READ_results. It contains four columns: the pair of individuals, the normalized mean P0 score for that pair and two columns showing how many standard errors that normalized mean P0 score is from a higher class of genetic difference (Z_upper, third column = distance to lower degrees of relationship) and a lower class of genetic difference (Z_lower, fourth column = distance to higher degrees of relationship). These values can be used to assess the certainty of the classification. We observed in our simulations that false classifications were enriched when the normalized mean P0 score were less than one standard error from the nearest threshold (i.e. |Z|<1). Additionally, a graphical representation of the results is produced (READ_results_plot.pdf) showing the results as well as uncertainties of individual estimates.

meansP0_AncientDNA_normalized is mainly for READ's internal use but it can be used for normalization with a user-defined value (see below).

#### Normalization ####

To scale the classification cutoffs for the expected distances between two unrelated individuals from the same population and using the same SNP panel, READ performs a normalization step using an estimate for this pairwise distance of unrelated individual. This helps to overcome potential biases arising from e.g. general population diversity or SNP ascertainment. Obtaining a good estimate is cruicial for the accuracy of READ but it is also difficult to formulate these expectations as large population samples are usually not available in aDNA studies. In default setting, READ estimates the normalization value from the input data which worked quite well in our test runs for sample sizes of 4 or more. READ calculates all pairwise distances between individuals. Based on the assumption that most pairs of individuals in a sample are unrelated, READ then uses the median of all pairwise differences as an estimate of the expected distance between two unrelated individuals.

This default setting (median of the sample -- **median**) may not be the best choice in all cases. Some studies may expect that the majority of all pairs are in fact related (e.g. when analyzing parents-child trios or larger pedigrees) or the sample size may be too small (e.g. 2). For trios, using the maximum value (**max**) of all three pairs may be a sensible choice as two of the three pairs would be expected to be first degrees (parent-child) while only one pairwise comparison (mother-father) would represent unrelated individuals. In other cases, such as sample sizes of only two, a normalization value can be obtained from a second (but genetically similar) population. To use this last approach, READ needs to be run twice: first on a dataset containing the genotype information from the population used to estimate the normalization value and second on the actual test population. For the first run, READ can be run in default settings. READ will output a file called meansP0_AncientDNA_normalized. The user can obtain a value for normalization from the NonNormalizedP0 column (e.g. the median). For the test population, READ then needs to be run with the following command: python READ.py example value <normalization_value> where <normalization_value> has to be replaced with the value obtained from the first run.

In summary, to use READ with a normalization approach different from the default setting (median), one would have to run READ like:

python READ.py example <normalization_method> <normalization_value>

A normalization is only required when a user-defined values is used instead of the median, mean or maximum of the test population. All normalization settings are described below:

* **median** (default) - assuming that most pairs of compared individuals are unrelated, READ uses the median across all pairs for normalization.
* **mean** - READ uses the mean across all pairs for normalization, this would be more sensitive to outliers in the data (e.g. recent migrants or identical twins)
* **max** - READ uses the maximum across all pairs for normalization. This should be used to test trios where both parents are supposed to be unrelated but the offspring is a first degree relative to both others.
* **value** <val> - READ uses a user-defined value for normalization. This can be used if the value from another population should be used for normalization. That would be useful if only two individuals are available for the test population. A value can be obtained from the NonNormalizedP0 column of the file meansP0_AncientDNA_normalized from a previous run of READ.

Optionally, one can add --window_size <value> at the end in order to modify the window size used (default: 1000000). We did not explore alternative window sizes in the paper.

## Contact ##

* Please contact Torsten G�nther (torsten.guenther *at* ebc.uu.se) or Jose Manuel Monroy Kuhn (jose.manuel.monroy *at* evobio.eu) if you have any further questions.
